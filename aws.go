package logging

const (
	AwsSqsQueueUrl               = "aws.sqs.queue.url"
	AwsSqsMessageID              = "aws.sqs.message.id"
	AwsSqsMessageSequenceNumber  = "aws.sqs.message.sequence_number"
	AwsSqsMessageDeduplicationId = "aws.sqs.message.deduplication_id"
	AwsSqsMessageGroupId         = "aws.sqs.message.group_id"
	AwsSqsMessageReceiptHandle   = "aws.sqs.message.receipt_handle"
)
