package logging

const (
	// Log keys
	SlackFeature           = "slack.feature"
	SlackSlashCommandName  = "slack.slash_command.name"
	SlackInteractionID     = "slack.interaction.id"
	SlackInteractionType   = "slack.interaction.type"
	SlackTeamID            = "slack.team.id"
	SlackAppID             = "slack.app.id"
	SlackServiceName       = "slack.app.service"
	SlackApiRateLimitStart = "slack.api.rate_limit.start"
	SlackEventType         = "slack.event.type"
	SlackChannelIDKey      = "slack.channel.id"
	SlackReactionNameKey   = "slack.reaction.name"
)
